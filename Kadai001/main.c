//
//  main.c
//  Kadai001
//
//  Created by Sakaki on 2017/03/06.
//  Copyright © 2017年 Sakaki. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//TextDate　Store
typedef struct{
    int number;//data_number
    char name[50];//data_name
    int age;//data_age
}TextData;


int main(int argc, const char * argv[]) {

    FILE *fin;
    char str[256];
    
    //Input TextDate
    TextData txdata[100];
    //Input TextDate Statement
    int inputstate=0;
    //for
    int i;
    
    //error check
    if( (fin = fopen("FileData.txt", "r") ) == NULL) {
        printf("Input file open error.\n");
        return 1;
    }
    
    //ReadFile
    while(fgets(str, sizeof(str), fin) != NULL) {
        //division sentence
        txdata[inputstate].number=atoi(strtok(str, ","));
        strcpy(txdata[inputstate].name,strtok(NULL, ","));
        txdata[inputstate].age=atoi(strtok(NULL, ","));
        inputstate++;
    }
    
    //close file
    fclose(fin);
    
    //show data
    printf("-------------------------------\n No\tName\t\tAge \n--------------------------------\n");
    for(i=0; i<inputstate; i++){
        printf("%d\t%s\t%d\n",txdata[i].number,txdata[i].name,txdata[i].age);
    }
    return 0;
}
